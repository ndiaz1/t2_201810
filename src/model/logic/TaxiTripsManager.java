package model.logic;

import api.ITaxiTripsManager;

import model.vo.Taxi;
import model.vo.Service;
import model.data_structures.LinkedList;
import model.data_structures.Lista;
import model.data_structures.Nodo;

import java.io.FileNotFoundException;
import java.io.FileReader;

import com.google.gson.JsonArray;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;

public class TaxiTripsManager implements ITaxiTripsManager {

	private Lista<Service> lista; 
	
	public TaxiTripsManager()
	{
		lista = new Lista<Service>();
	}
	
	public void loadServices (String serviceFile) {
		// TODO Auto-generated method stub
		System.out.println("Inside loadServices with " + serviceFile);
		
		JsonParser parser = new JsonParser();
	try{
	JsonArray arr = (JsonArray) parser.parse(new FileReader(serviceFile));
		for (int i = 0; arr != null && i < arr.size(); i++)
		{
			JsonObject obj= (JsonObject) arr.get(i);
			/* Mostrar un JsonObject (Servicio taxi) */
			System.out.println("------------------------------------------------------------------------------------------------");
			System.out.println(obj);
			
			String id = "Unknown";
			if ( obj.get("taxi_id") != null )
			{ id = obj.get("taxi_id").getAsString();}
			
			String comp = "Unknown";
			if ( obj.get("company") != null )
			{ comp = obj.get("company").getAsString();}
			
			String dca = "Unknown";
			if ( obj.get("dropoff_community_area") != null )
			{ dca = obj.get("dropoff_community_area").getAsString(); }
			
			String tripid = "Unknown";
			if ( obj.get("trip_id") != null )
			{ tripid = obj.get("trip_id").getAsString(); }
			
			int tripseconds = -1;
			if ( obj.get("trip_seconds") != null )
			{ tripseconds = obj.get("trip_seconds").getAsInt(); }
			
			double tripmiles = -1;
			if ( obj.get("trip_miles") != null )
			{ tripmiles = obj.get("trip_miles").getAsDouble(); }
			
			double triptotal = -1;
			if ( obj.get("trip_total") != null )
			{ tripmiles = obj.get("trip_total").getAsDouble(); }
			
			Service a = new Service(id, comp, dca, tripid, tripseconds, tripmiles, triptotal);
			lista.agregar(a);
		}
		System.out.println("\nSe cargaron "+lista.darTamano()+" elementos.\n");
		System.out.println("\n"+"FORMATO (Ejemplo): "+lista.darPrimero().darElemento().toString()+"\n");
	}
	catch (JsonIOException e1 ) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
	catch (JsonSyntaxException e2) {
		// TODO Auto-generated catch block
		e2.printStackTrace();
	}
	catch (FileNotFoundException e3) {
		// TODO Auto-generated catch block
		e3.printStackTrace();
	}
	}

	@Override
	public LinkedList<Taxi> getTaxisOfCompany(String company) {
		System.out.println("\nInside getTaxisOfCompany with " + company);
		Lista<Taxi> taxlist = darListaTaxis();
		Lista<Taxi> resp = new Lista<Taxi>();
		Nodo<Taxi> actual = taxlist.darPrimero();
		while(actual != null)
		{
			if(actual.darElemento().getCompany().equals(company))
			{
				resp.agregar(actual.darElemento());
			}
			actual = actual.darSiguiente();
		}
		return resp;
	}
	
	public boolean taxiExistente(String id, Lista<Taxi> list)
	{
		boolean rta=false;
		Nodo<Taxi> actual = list.darPrimero();
		while(actual != null && !rta)
		{
			if(actual.darElemento().getTaxiId().equals(id))
			{
				rta = true;
			}
			actual = actual.darSiguiente();
		}
		return rta;
	}
	
	public Lista<Taxi> darListaTaxis()
	{
		Lista<Taxi> res = new Lista<Taxi>();
		Nodo<Service> actual = (Nodo<Service>) lista.darPrimero();
		while(actual !=null)
		{
			if(!taxiExistente(actual.darElemento().getTaxiId(),res))
			{
				Taxi nuevo = new Taxi(actual.darElemento().getTaxiId(), actual.darElemento().getCompany());
				res.agregar(nuevo);
			}
			actual = actual.darSiguiente();
		}
		
		return res;
	}

	@Override
	public LinkedList<Service> getTaxiServicesToCommunityArea(int communityArea) {
		System.out.println("\nInside getTaxiServicesToCommunityArea with " + communityArea);
		Lista<Service> res = new Lista<Service>();
		Nodo<Service> actual = (Nodo<Service>) lista.darPrimero();
		while(actual !=null)
		{
			String[] c = actual.darElemento().toString().split("----");
			if(!c[2].equals("Unknown") && Integer.parseInt(c[2]) == communityArea)
			{
				res.agregar(actual.darElemento());
			}
			actual = actual.darSiguiente();
		}
		return res;
	}


}
