package model.vo;

import model.data_structures.Lista;
import model.data_structures.Nodo;

/**
 * Representation of a taxi object
 */
public class Taxi implements Comparable<Taxi>{
	
	private String taxi_id;
	private String company;
	
	public Taxi(String id, String comp){
		taxi_id = id;
		company = comp;
	}


	/**
	 * @return id - taxi_id
	 */
	public String getTaxiId() {
		// TODO Auto-generated method stub
		return taxi_id;
	}

	/**
	 * @return company
	 */
	public String getCompany() {
		// TODO Auto-generated method stub
		return company;
	}
	
	
	@Override
	public int compareTo(Taxi o) {
		// Compara por taxi_id
		int c = 0;
		String[] array = this.toString().split("-");
		String[] arrayO= o.toString().split("-");
		if(array[0].compareTo(arrayO[0])>0)
		{
			c=1;
		}
		else if(array[0].compareTo(arrayO[0])<0)
		{
			c=-1;
		}
		return c;
	}	
	
	public String toString(){
		return taxi_id + "----" + company;
	}
}
