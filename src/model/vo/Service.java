package model.vo;

/**
 * Representation of a Service object
 */
public class Service implements Comparable<Service> {
	
	private String taxi_id;
	private String company;
	private String dropoff_community_area;
	private String trip_id;
	private int trip_seconds;
	private double trip_miles;
	private double trip_total;
	
	public Service(String id, String comp, String dca, String tripid, int tripseconds, double tripmiles, double triptotal){
		taxi_id=id;
		company=comp;
		dropoff_community_area=dca;
		trip_id=tripid;
		trip_seconds=tripseconds;
		trip_miles=tripmiles;
		trip_total=triptotal;
	}
	
	/**
	 * @return company
	 */
	public String getCompany() {
		// TODO Auto-generated method stub
		return company;
	}
	
	public String getCommunityArea(){
		return dropoff_community_area;
	}
	
	/**
	 * @return id - Trip_id
	 */
	public String getTripId() {
		// TODO Auto-generated method stub
		return trip_id;
	}	
	
	/**
	 * @return id - Taxi_id
	 */
	public String getTaxiId() {
		// TODO Auto-generated method stub
		return taxi_id;
	}	
	
	/**
	 * @return time - Time of the trip in seconds.
	 */
	public int getTripSeconds() {
		// TODO Auto-generated method stub
		return trip_seconds;
	}

	/**
	 * @return miles - Distance of the trip in miles.
	 */
	public double getTripMiles() {
		// TODO Auto-generated method stub
		return trip_miles;
	}
	
	/**
	 * @return total - Total cost of the trip
	 */
	public double getTripTotal() {
		// TODO Auto-generated method stub
		return trip_total;
	}

	@Override
	public int compareTo(Service o) {
		// Compara por taxi_id
		int x=0;
		String[] a = this.toString().split("----");
		String[] b = o.toString().split("----");
		if(a[0].compareTo(b[0])>0)
		{
			x = 1;
		}
		else if(a[0].compareTo(b[0])<0)
		{
			x = -1;
		}
		else if(a[0].compareTo(b[0])==0)
		{
			x = 0;
		}
		return x;
	}
	
	public String toString()
	{
		return taxi_id+"----"+company+"----"+dropoff_community_area;
	}
}
