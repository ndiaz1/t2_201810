package model.data_structures;

public class Nodo<T> {
	
	private Nodo<T> siguiente;
	
	private T elemento;
	
	public Nodo(T elem)
	{
		elemento = elem;
		siguiente = null;
	}
	
	public T darElemento()
	{
		return elemento;
	}
	
	public Nodo<T> darSiguiente(){
		return siguiente;
	}
	
	public void cambiarSiguiente(Nodo<T> nuevo){
		siguiente = nuevo;
	}
}
