package model.data_structures;

public class Lista<T extends Comparable<T>> implements LinkedList<T> {
	
	private Nodo<T> primero;
	
	private Nodo<T> ultimo;
	
	private int longitud;
	
	public Lista(){
		primero = null;
		ultimo = null;
		longitud = 0;
	}
	
	public void agregar(T n)
	{
		Nodo<T> nuevo = new Nodo<T>(n);
		if(primero == null)
		{
			primero = nuevo;
			ultimo = nuevo;
			longitud++;
		}
		else
		{
			ultimo.cambiarSiguiente(nuevo);
			ultimo = nuevo;
			longitud ++;
		}
	}
	
	public void eliminar(T e)
	{
		boolean elim = false;
		if(e.equals(primero) && !elim)
		{
			primero = primero.darSiguiente();
			elim = true;
		}
		else
		{
			Nodo<T> actual = primero;
			while(actual != null && !elim)
			{
				if(actual.darSiguiente().equals(e))
				{
					actual.cambiarSiguiente(actual.darSiguiente().darSiguiente());
					elim = true;
				}
				else
				{
					actual = actual.darSiguiente();
				}
			}
		}
	}
	
	public T dar(T e)
	{
		T elem = null;
		if(primero.darElemento().equals(e))
		{
			elem = primero.darElemento();
		}
		else
		{
			Nodo<T> actual = primero.darSiguiente();
			while(actual != null)
			{
				if(actual.darElemento().equals(e))
				{
					elem = actual.darElemento();
				}
				else
				{
					actual = actual.darSiguiente();
				}
			}
		}
		return elem;
	}
	
	public Nodo<T> darPrimero(){
		return primero;
	}
	
	public Nodo<T> darUltimo(){
		return ultimo;
	}
	
	public int darTamano()
	{
		return longitud;
	}
}
